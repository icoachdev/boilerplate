## Node + EJS boilderplate

For quick static sites.

### Install and Run
```
npm install
```
```
npm start
```

### Routing
Each new page you add needs to be a folder with an ```index.ejs``` file in it inside of the ```src/``` directory. Then you can access the page via the folder route. See below for example:

Folder structure:
```
.src/
...index.ejs 
...2/
......index.ejs 
...course/
......index.ejs 
...course/2/
.........index.ejs

```

If you wanted to navigate to the ```course``` page in the browser you would use ```http://localhost:8000/course/```

If you wanted to navigate to the second version of the  ```course``` page in the browser you would use ```http://localhost:8000/course/2/``` etc.


### iCoach Client's Site Templating
Details about iCoach specific templates and client's site creation process you will find at [SRC_README](src/README.md)

### Assets
All static assets / images can be put in the "images" folder. They can then be referenced on your pages like so:
```html
<img src="/images/my-image.jpeg" />
```

### Node Modules
If you want to distribute any node modules, you will need to add them to the "buildNodeModules" function in the gulp task file ```gulpfile.js```.

You need to specify the path in the ```node_modules``` folder to make. See below example:
```javascript
const modules = [
  {
    module: './node_modules/path/to/mymodule/file.min.js',
    dest: './dist/js/modules/',
  },
];
```
Gulp will make a copy into the local ```dist``` folder so it can be  served. It can then be referenced using the ```path``` variable in ejs like this:
```html
<script src="<%- path %>js/modules/file.min.js"></script>
```

### Distributing
When you're ready to distribute the goods, run the below command. This will compile/transpile/minimize everything and place all in the ```dist/``` folder.
```
gulp build-dist
```

### Gulp Tasks
Gulp tasks will run automatically but to manually run them:

Building Javascript: This will concat the js files together in order by name:
```
gulp build-js
```

Building SASS
```
gulp build-sass
```

Compressing Images
```
gulp build-images
```

Using JS Node Modules on the client
```
gulp build-js-modules
```

### This boilerplate includes the following
* Gulp (task runner)
* EJS (templating)
* BrowserSync (live reloading)
* SASS (css preprocessor)
* ES6 (gulp-babel)
* eslint (js linting)
